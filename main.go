package main

import (
	"fmt"
	"log"
	"os"
	"os/signal"
	"syscall"
	"time"
)

func main() {
    fmt.Println("Hello, World!")

    go publish()

    quitChannel := make(chan os.Signal, 1)
	signal.Notify(quitChannel, syscall.SIGINT, syscall.SIGTERM)

    <-quitChannel
}

func publish() {
	i := 1
	for {
		log.Println("Enviando evento")
		log.Printf("Event - %v", i)

		i++
		time.Sleep(time.Microsecond)

		if i >= 200000 {
			break
		}
	}
}
